# Provision New AWS Accounts

Project for provisioning new AWS accounts.

## Description

This solution uses AWS Service Catalog to automate the provisioning of new AWS accounts and setting up some baseline for new account. The pipeline has 3 stages -

1. create-new-account
2. configure-sso-access
3. configure-account-baseline

### create-new-account

As a prerequisite the Control Tower should already be setup before using it. The shell script create-account.sh first assumes a role in the management account . It then gets the product id with name 'AWS Control Tower Account Factory'. After that it gets the provisioning artifact id and gets the required parameters for the new account from parameter.json to create a new account.

### configure-sso-access

In this stage we configure SSO access for the following groups. It enables users in these groups to get access to the newly created account.

1. Read Only
2. Power User
3. Administrator

### configure-account-baseline

In the final stage of the pipeline we configure the baseline for the new account. For the sake of demonstration I create a new VPC and a new IAM policy in the account. You may setup additional resources like s3 bucket, subnets, Ec2 instances, enables AWS Config etc. as per your requirement.

## Installation

Fork this repo and provide the required parameters for a new account like - AccountName, AccountEmail, SSOUserFirstName, SSOUserLastName, SSOUserEmail. Once you push your changes to the main branch it kicks off the Gitlab pipeline. When all the stages of the pipeline are completed successfully you may find the new account in AWS Control Tower or Organizations or SSO.

## Contributing

Merge Requests are most welcome. Make sure to use the latest code from the Main branch and please provide a detailed description of your changes.

## References

https://aws.amazon.com/blogs/mt/automate-account-creation-and-resource-provisioning-using-aws-service-catalog-aws-organizations-and-aws-lambda/

https://aws.amazon.com/blogs/mt/how-to-automate-the-creation-of-multiple-accounts-in-aws-control-tower/

https://aws.amazon.com/blogs/security/how-to-automate-aws-account-creation-with-sso-user-assignment/
