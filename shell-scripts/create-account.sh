#!/bin/bash

#dot command to import the function get_temporary_credentials() to current execution context
. ./get-temp-credentials.sh

product_id=$(aws servicecatalog search-products --filters FullTextSearch="AWS Control Tower Account Factory" | jq .ProductViewSummaries[0].ProductId | tr -d '"')

provisioning_artifact_id=$(aws servicecatalog describe-product --id prod-raltrflxdtt7u | jq .ProvisioningArtifacts[0].Id | tr -d '"')

provisioned_product_name=$(jq '.[] | select(.Key == "AccountName")| .Value' ../parameters.json | tr -d '"')

aws servicecatalog provision-product \
--product-id "$product_id" \
--provisioning-artifact-id "$provisioning_artifact_id" \
--provisioned-product-name "$provisioned_product_name" \
--provisioning-parameters file://../parameters.json

