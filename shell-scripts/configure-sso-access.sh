#!/bin/bash

#dot command to import the function get_temporary_credentials() to current execution context
. ./get-temp-credentials.sh  

account_email=$(jq '.[] | select(.Key == "AccountEmail")| .Value' ../parameters.json | tr -d '"')

export account_email

account_number=$(aws organizations list-accounts | jq -r '.Accounts[] | select(.Email == env.account_email) | .Id')

sso_instance_arn=arn:aws:sso:::instance/ssoins-7223acaf8034a53b

administrator_access=arn:aws:sso:::permissionSet/ssoins-7223acaf8034a53b/ps-71d5ced237d55373
read_only_access=arn:aws:sso:::permissionSet/ssoins-7223acaf8034a53b/ps-c5040aacbbefbd4a
power_user_access=arn:aws:sso:::permissionSet/ssoins-7223acaf8034a53b/ps-780c9cee07f49265

read_only_ccess_group=9067679599-7883130a-70ef-4b08-a438-1d652298c21e
power_user_access_group=9067679599-44065a2f-c278-4a43-bad6-121bc1d8c99f
administrator_access_group=9067679599-9dd858aa-df56-482a-a707-a0df945daa2f

aws sso-admin create-account-assignment \
--instance-arn "$sso_instance_arn" \
--target-id "$account_number" \
--target-type AWS_ACCOUNT \
--permission-set-arn "$read_only_access" \
--principal-type GROUP \
--principal-id "$read_only_ccess_group"

aws sso-admin create-account-assignment \
--instance-arn "$sso_instance_arn" \
--target-id "$account_number" \
--target-type AWS_ACCOUNT \
--permission-set-arn "$power_user_access" \
--principal-type GROUP \
--principal-id "$power_user_access_group"

aws sso-admin create-account-assignment \
--instance-arn "$sso_instance_arn" \
--target-id "$account_number" \
--target-type AWS_ACCOUNT \
--permission-set-arn "$administrator_access" \
--principal-type GROUP \
--principal-id "$administrator_access_group"
