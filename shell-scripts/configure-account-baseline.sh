#!/bin/bash

#dot command to have same execution context
. ./get-temp-credentials.sh

account_name=$(jq '.[] | select(.Key == "AccountName")| .Value' ../parameters.json | tr -d '"')
account_email=$(jq '.[] | select(.Key == "AccountEmail")| .Value' ../parameters.json | tr -d '"')

export account_email

account_number=$(aws organizations list-accounts | jq -r '.Accounts[] | select(.Email == env.account_email) | .Id')

region="us-east-1"
template_directory="../cloud-formation-templates/"
administration_role_arn="arn:aws:iam::975300453774:role/aws-service-role/stacksets.cloudformation.amazonaws.com/AWSServiceRoleForCloudFormationStackSetsOrgAdmin"
execution_role_name="stacksets-exec-7c1c4bf64c7200a2812171d64ed5a73e"
# auto_deployment="Enabled=false,RetainStacksOnAccountRemoval=false"
deployment_targets="Accounts=$account_number"


create_stack_set()
{
    template=$template_directory$1".yml"
    stack_set_name=$2

    aws cloudformation create-stack-set \
    --stack-set-name "$stack_set_name" \
    --template-body file://"$template_directory""$template" \
    --administration-role-arn "$administration_role_arn" \
    --execution-role-name "$execution_role_name" \
    --permission-model "SELF_MANAGED" \
    --capabilities "CAPABILITY_NAMED_IAM" \
    --managed-execution "Active=true"

    aws cloudformation create-stack-instances \
    --stack-set-name "$stack_set_name" \
    --deployment-targets "$deployment_targets" \
    --regions "$region"
}


templates=$(ls "$template_directory" | grep .yml | cut -d "." -f1)

for template in $templates; do
    stack_set_name=$account_name"-"$template
    echo "create stack set: $stack_set_name"
    create_stack_set "$template" "$stack_set_name"
done