#!/bin/bash

get_temporary_credentials()
{
    echo "get temporary credentials"
    aws sts get-caller-identity

    role_arn="arn:aws:iam::975300453774:role/provision-aws-account"

    temp_creds=(`aws sts assume-role --role-arn $role_arn --role-session-name pipeline --query '[Credentials.AccessKeyId, Credentials.SecretAccessKey, Credentials.SessionToken]' --output text`)

    export AWS_DEFAULT_REGION="us-east-1"
    export AWS_ACCESS_KEY_ID=${temp_creds[0]}
    export AWS_SECRET_ACCESS_KEY=${temp_creds[1]}
    export AWS_SESSION_TOKEN=${temp_creds[2]}

    aws sts get-caller-identity

    # temp_creds=$(aws sts assume-role --role-arn $role_arn --role-session-name pipeline)

    # AccessKeyId=$(echo "$temp_creds" | jq .Credentials.AccessKeyId)
    # SecretAccessKey=$(echo "$temp_creds" | jq .Credentials.SecretAccessKey)
    # SessionToken=$(echo "$temp_creds" | jq .Credentials.SessionToken)

    # export AWS_DEFAULT_REGION="us-east-1"
    # export AWS_ACCESS_KEY_ID=$AccessKeyId
    # export AWS_SECRET_ACCESS_KEY=$SecretAccessKey
    # export AWS_SESSION_TOKEN=$SessionToken
}

get_temporary_credentials